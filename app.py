from flask import Flask, render_template, request
from flask import jsonify
from wtforms import TextField, Form
import requests


"""
Hi! 

Before starting work, I read the documentation and understood that the problem can be solved differently.

I. First I started doing it using the Giant Bomb API and I wanted to make autocomplete in the input. 
But due to the fact that the API KEY had a limited number of requests for the user, I left this decision. 
Because each time entering a new letter in the word - we make a request to the API. 
I assume that after a few requests my API KEY will be blocked.

II. Then there was the idea of ​​throwing an idea with autocomplete. 
And just pass the "search word" to the handler and then output "static data", 
but i have not many time and I left this decision to.

III. To tell you the truth, I overestimated the time of the task and the Giant Bomb API, and I started acting radically. 
I needed the skills that I used when writing parsers.
I analyzed the search requests of the site. 
Looked at what data the client sends and what data it receives from the server. 
Simulated the requested query and processed the response. 
After that he structured the information and got a solution different from test task.

p.s .: I understand that the task can be performed partially and differ from the set conditions (without taking into account the platform). 
So sorry, i used more "lazy" way. Have a nice day.

Regards

Alex
"""


app = Flask(__name__)


class SearchForm(Form):
    autocomp = TextField('autocomp', id='autocomplete', render_kw={"placeholder": "just typing something"})

@app.route('/autocomplete', methods=['GET'])
def autocomplete():
    word = request.args.get('word').lower()

    games = requests.get(
        'https://www.giantbomb.com/jsonsearch/?header=1&module=autocomplete&q='+str(word).lower(),
        headers={
            'authority': 'www.giantbomb.com',
            'method': 'GET',
            'path': '/jsonsearch/?header=1&module=autocomplete&q='+str(word).lower(),
            'scheme': 'https',
            'accept': 'application/json, text/javascript, */*; q=0.01',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'ru,en-US;q=0.9,en;q=0.8,uk;q=0.7',
            'cookie': 'sat=e8cfaa0909ce1b857389f324ca5c387f; device_view=full; sptg=%5B%5D; AD_SESSION=p; skinSource=giantbomb_white; XCLGFbrowser=XRvpp1tqvj6foNMhsfs; xcab=1-0; __utmc=223628573; __utmz=223628573.1533725905.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); LDCLGFbrowser=e5889b7d-b78b-477e-aad6-92cfed4d393e; AMCVS_10D31225525FF5790A490D4D%40AdobeOrg=1; s_cc=true; aam_uuid=40620024780964018224395861530344804494; __utmv=223628573.|2=User%20Type=Registered=1^3=Subscription%20Type=None=1; trc_cookie_storage=taboola%2520global%253Auser-id%3D2c64ed55-8e21-462a-885b-a9e8949287d0; pat=UGhvZW5peFxVc2VyQnVuZGxlXEVudGl0eVxVc2VyOllXeGxlR0p5WlhWejoxODQ5MDg2NjkwOmNlMDgwNjk2ZWExYTc3ZDE0ZTYxMTA3YTIwNDdkYWM4ZjdiNzBmNjIxOTk5ZDAzODI0OGE2NTgwZmMyNjI2NzU%3D; s_sq=%5B%5BB%5D%5D; ads_firstpg=0; xcr=ua; s_lv_giantbomb_s=Less%20than%201%20day; s_vnum=1536317905338%26vn%3D4; s_invisit=true; __utma=223628573.875728244.1533725905.1533828552.1533830446.5; __utmt=1; __utmb=223628573.2.10.1533830446; AMCV_10D31225525FF5790A490D4D%40AdobeOrg=-894706358%7CMCMID%7C40376654008173529344419920422391911926%7CMCAAMLH-1534330705%7C6%7CMCAAMB-1534435566%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCCIDH%7C181041699%7CMCOPTOUT-1533835751s%7CNONE%7CvVersion%7C2.3.0%7CMCAID%7C2DB5667985311518-6000010BA0000B6E; s_getNewRepeat=1533830771660-Repeat; s_lv_giantbomb=1533830771664',
            'referer': 'https://www.giantbomb.com/api/',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36',
            'x-newrelic-id': 'Vg8OUFFACQQBUFVbDg==',
            'x-requested-with': 'XMLHttpRequest'
        }
    ).json()['results']

    for game in games:
        game['image'] = game['image'].replace('square_mini', 'scale_small')

    return jsonify(json_list=games)


@app.route('/', methods=['GET', 'POST'])
def hello_world():
    form = SearchForm(request.form)

    return render_template('index.html', title='Find your game', form=form)


if __name__ == '__main__':
    app.run()
